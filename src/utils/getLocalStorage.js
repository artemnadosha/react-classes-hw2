export const getLocalStorage = (key) => {
  const item = window.localStorage.getItem(key);
  if (!!item) {
    return JSON.parse(item);
  }

  return false;
};

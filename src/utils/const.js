const FIRST_COLOR = "#03c139";
const SECOND_COLOR = "#dc3535";

const MODAL_FIRST_CONTENT = {
  HEADER: "Do you want to delete this file?",
  TEXT: "Once you delete this file, it won't be possible to undo this action. Are you sure you want yo delete it?",
};

const MODAL_SECOND_CONTENT = {
  HEADER: "Lorem ipsum dolor sit amet.",
  TEXT: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio harum molestiae perferendis quos saepe tempore.",
};

const LOCALSTORAGE_CONST = {
  FAVORITE: "favorite",
  CART: "cart",
};

export {
  FIRST_COLOR,
  SECOND_COLOR,
  MODAL_SECOND_CONTENT,
  MODAL_FIRST_CONTENT,
  LOCALSTORAGE_CONST,
};

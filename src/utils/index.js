export { getLocalStorage } from "./getLocalStorage";
export { setLocalStorage } from "./setLocalStorage";
export {
  FIRST_COLOR,
  SECOND_COLOR,
  MODAL_SECOND_CONTENT,
  MODAL_FIRST_CONTENT,
  LOCALSTORAGE_CONST,
} from "./const";

import { Component } from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";
import { FIRST_COLOR, SECOND_COLOR } from "../../utils";
class Button extends Component {
  render() {
    const { children, backgroundColor, onClick } = this.props;
    return (
      <button
        className={styles.button}
        onClick={onClick}
        style={{ backgroundColor: backgroundColor }}
      >
        <p>{children}</p>
      </button>
    );
  }
}

export default Button;

Button.propTypes = {
  backgroundColor: PropTypes.oneOf([`${FIRST_COLOR}`, `${SECOND_COLOR}`])
    .isRequired,
  onClick: PropTypes.func,
  children: PropTypes.string,
};

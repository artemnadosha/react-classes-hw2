import { Component } from "react";
import styles from "./App.module.scss";
import ProductList from "./components/ProductsList/ProductList";
import { getLocalStorage, LOCALSTORAGE_CONST } from "./utils";
import { ReactComponent as CartIcon } from "./assets/img/cartIcon.svg";
import { ReactComponent as StarIcon } from "./assets/img/starIcon.svg";

class App extends Component {
  state = {
    products: [],
    quantityFavorite: 0,
    quantityCart: 0,
  };

  fetchCards = async () => {
    const res = await fetch("./product.json");
    const json = await res.json();

    if (!!json) {
      this.setState({ products: json.products.slice(0, 10) });
    }
  };

  handleUpdateQuantityFavorite = () => {
    this.setState({
      quantityFavorite: getLocalStorage(LOCALSTORAGE_CONST.FAVORITE).length,
    });
  };
  handleUpdateQuantityCart = () => {
    this.setState({
      quantityCart: getLocalStorage(LOCALSTORAGE_CONST.CART).length,
    });
  };

  componentDidMount() {
    this.fetchCards();
    this.handleUpdateQuantityCart();
    this.handleUpdateQuantityFavorite();
  }

  render() {
    const { products, quantityCart, quantityFavorite } = this.state;

    return (
      <div className={styles.app}>
        <div className={styles.wrapperActions}>
          <div
            className={styles.actions}
            style={quantityFavorite > 0 ? { color: "gold" } : { color: "gray" }}
          >
            {<StarIcon />} {quantityFavorite}
          </div>
          <div
            className={styles.actions}
            style={
              quantityCart > 0 ? { color: "lightblue" } : { color: "gray" }
            }
          >
            {<CartIcon />} {quantityCart}
          </div>
        </div>
        <ProductList
          products={products}
          handleUpdateQuantityFavorite={this.handleUpdateQuantityFavorite}
          handleUpdateQuantityCart={this.handleUpdateQuantityCart}
        />
      </div>
    );
  }
}

export default App;

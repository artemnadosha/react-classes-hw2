import { getLocalStorage } from "../../../utils";

export const checkActivityLocalStorage = (id, key) => {
  const product = getLocalStorage(key);

  if (!!product) {
    const check = product.find((item) => item.id === id);

    return !!check;
  }
};

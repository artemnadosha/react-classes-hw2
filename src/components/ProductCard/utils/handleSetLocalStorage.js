import { getLocalStorage, setLocalStorage } from "../../../utils";

export const handleSetLocalStorage = ({ title, price, thumbnail, id, key }) => {
  const localFavoriteItem = getLocalStorage(key);
  const product = {
    title: title,
    price: price,
    thumbnail: thumbnail,
    id: id,
  };

  if (!!localFavoriteItem?.length) {
    const addProductLocalStorage = [...localFavoriteItem, product];
    setLocalStorage(key, addProductLocalStorage);
  } else {
    setLocalStorage(key, [product]);
  }
};

import { Component } from "react";
import PropTypes from "prop-types";
import { Button } from "../../UI";
import { ReactComponent as StarIcon } from "../../assets/img/starIcon.svg";
import styles from "./ProductCard.module.scss";
import { FIRST_COLOR, LOCALSTORAGE_CONST } from "../../utils";
import { checkActivityLocalStorage, handleSetLocalStorage } from "./utils";
import PurchaseConfirmationModal from "../PurchaseConfirmationModal/PurchaseConfirmationModal";

class ProductCard extends Component {
  state = {
    starActive: false,
    starColor: "gray",
    modalAction: false,
    cart: false,
  };
  handleOpenModal = () => {
    this.setState({ modalAction: true });
  };
  handleCloseModal = () => {
    this.setState({ modalAction: false });
  };

  handleStarActive = () => {
    this.setState({ starActive: true, starColor: "gold" });
  };

  checkActivityFavorite = () => {
    const activityFavorite = checkActivityLocalStorage(
      this.props.id,
      LOCALSTORAGE_CONST.FAVORITE
    );

    if (activityFavorite) {
      this.setState({ starActive: true, starColor: "gold" });
    }
  };

  handleStarAction = () => {
    this.handleStarActive();
    handleSetLocalStorage({
      title: this.props.title,
      price: this.props.price,
      thumbnail: this.props.thumbnail,
      id: this.props.id,
      key: LOCALSTORAGE_CONST.FAVORITE,
    });
    this.props.handleUpdateQuantityFavorite();
  };

  handleOnSubmitModal = () => {
    handleSetLocalStorage({
      title: this.props.title,
      price: this.props.price,
      thumbnail: this.props.thumbnail,
      id: this.props.id,
      key: LOCALSTORAGE_CONST.CART,
    });
    this.setState({ cart: true });
    this.props.handleUpdateQuantityCart();
    this.handleCloseModal();
  };

  componentDidMount() {
    this.checkActivityFavorite();
  }

  render() {
    const { title, price, thumbnail: image } = this.props;
    const { starColor, starActive, modalAction, cart } = this.state;

    return (
      <div className={styles.wrapperCard}>
        <div className={styles.wrapperImage}>
          <img className={styles.image} src={image} alt={title} />
        </div>
        <div className={styles.wrapperInfo}>
          <p>{title}</p>
          <p>${price}</p>
        </div>
        <div className={styles.wrapperActions}>
          <div
            className={styles.wrapperStar}
            onClick={
              starActive
                ? () => {
                    alert("The product has already been added");
                  }
                : this.handleStarAction
            }
          >
            <StarIcon width="24px" height="24px" color={starColor} />
          </div>
          <Button
            backgroundColor={FIRST_COLOR}
            onClick={
              cart
                ? () => {
                    alert("You have already added to cart");
                  }
                : this.handleOpenModal
            }
          >
            Add to cart
          </Button>
        </div>
        {modalAction && (
          <PurchaseConfirmationModal
            handleCloseModal={this.handleCloseModal}
            handleOnSubmitModal={this.handleOnSubmitModal}
          />
        )}
      </div>
    );
  }
}

ProductCard.defaultProps = {
  title: "title",
  price: 0,
  thumbnail: "#",
};

ProductCard.propTypes = {
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  thumbnail: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  handleUpdateQuantityCart: PropTypes.func.isRequired,
  handleUpdateQuantityFavorite: PropTypes.func.isRequired,
};

export default ProductCard;

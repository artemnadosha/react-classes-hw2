import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Modal } from "../../UI";
import { FIRST_COLOR, MODAL_FIRST_CONTENT, SECOND_COLOR } from "../../utils";

class PurchaseConfirmationModal extends Component {
  render() {
    const { handleCloseModal, handleOnSubmitModal } = this.props;

    return (
      <Modal
        onClose={handleCloseModal}
        header={MODAL_FIRST_CONTENT.HEADER}
        text={MODAL_FIRST_CONTENT.TEXT}
        closeButton={false}
      >
        <Button backgroundColor={FIRST_COLOR} onClick={handleOnSubmitModal}>
          Ok
        </Button>
        <Button backgroundColor={SECOND_COLOR} onClick={handleCloseModal}>
          Cancel
        </Button>
      </Modal>
    );
  }
}

PurchaseConfirmationModal.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  handleOnSubmitModal: PropTypes.func.isRequired,
};

export default PurchaseConfirmationModal;

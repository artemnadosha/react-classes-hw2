import { Component } from "react";
import PropTypes from "prop-types";
import styles from "./ProductList.module.scss";
import ProductCard from "../ProductCard/ProductCard";

class ProductList extends Component {
  render() {
    const { products, handleUpdateQuantityFavorite, handleUpdateQuantityCart } =
      this.props;

    return (
      <div className={styles.wrapperList}>
        {!!products?.length &&
          products.map((product) => (
            <ProductCard
              key={product.id}
              id={product.id}
              title={product.title}
              price={product.price}
              thumbnail={product.thumbnail}
              handleUpdateQuantityFavorite={handleUpdateQuantityFavorite}
              handleUpdateQuantityCart={handleUpdateQuantityCart}
            />
          ))}
      </div>
    );
  }
}

export default ProductList;

ProductList.propTypes = {
  products: PropTypes.array.isRequired,
  handleUpdateQuantityFavorite: PropTypes.func.isRequired,
  handleUpdateQuantityCart: PropTypes.func.isRequired,
};
